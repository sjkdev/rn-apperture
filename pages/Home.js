import React, {Component} from 'react';
import { StyleSheet, View, Button} from 'react-native';
import Home from './pages/Home';
import About from './pages/About';
import FAQ from './pages/FAQ';
import Contact from './pages/Contact';

class Home extends Component {
    render() {
        <View style={styles.container}>
            <Button title="go back to Home screen" onPress={() => this.props.navigate('Home')} />
            <Button title="go back to About screen" onPress={() => this.props.navigate('About')} />
            <Button title="go back to FAQ screen" onPress={() => this.props.navigate('FAQ')} />
            <Button title="go back to Contact screen" onPress={() => this.props.navigate('Contact')} />
        </View>
    }
}
export default Home;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });
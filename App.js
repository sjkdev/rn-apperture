import React, {Component} from 'react';
import { 
  StyleSheet, 
  Text, 
  View
} from 'react-native';

import { createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

// import Home from './pages/Home';
// import About from './pages/About';
// import FAQ from './pages/FAQ';
// import Contact from './pages/Contact';



export class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open Up App.js to start working with your app</Text>
      </View>
    );
  }
}

export class About extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>About</Text>
      </View>
    );
  }
}

export class FAQ extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>FAQ</Text>
      </View>
    );
  }
}

export class Contact extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Contact</Text>
      </View>
    );
  }
}

export default createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-home" color={tintColor} size={20} />
      )
    }
  },

  About: {
    screen: About,
    navigationOptions: {
      tabBarLabel: 'About',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-appstore" color={tintColor} size={20} />
      )
    }
  },

  FAQ: {
    screen: FAQ,
    navigationOptions: {
      tabBarLabel: 'FAQ',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-apps" color={tintColor} size={20} />
      )
    }
  },

  Contact: {
    screen: Contact,
    navigationOptions: {
      tabBarLabel: 'Contact',
      tabBarIcon: ({tintColor}) => (
        <Icon name="ios-chatboxes" color={tintColor} size={20} />
      )
    }
  },
 

}, {//router config
      initialRouteName: 'Home',
      order: ['Home', 'About', 'FAQ', 'Contact'],
      // navigation for complete tab navigator
      navigationOptions: {
        tabBarVisible: true
      },
      tabBarOptions: {
        activeTintColor: 'purple',
        inactiveTintColor: '#555'
      }
  });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});